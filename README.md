# This project is obsolete and no longer works.  For a replacement see https://gitlab.com/fartwhif/channelsuck

**subscriber**

*requirements:*

download chromium and chromedriver.  https://chromedriver.chromium.org/downloads

######    required arguments

`  -u, --user-path          Required. Chromium user data path. Example: C:\Users\user\AppData\Local\Chromium\User Data`

`  -d, --driver-path        Required. Path to folder containing chromedriver.exe Example:`
`                           C:\Users\user\Downloads\Win_999972_chrome-win\chrome-win`

######   Backup your Youtube subscriptions.
`  -o, --output-file        Required. File to save list of subscriptions.`

`  -f, --force-overwrite    If the output file exists, overwrite it without interactive confirmation.`
                           
######   Restore your Youtube subscriptions.
`  -i, --input-file         Required. Input file containing list of subscriptions to restore.`


