using OpenQA.Selenium;

namespace subscriber
{
    internal class Browser
    {
        public IWebDriver Chrome { get; set; }
        public IJavaScriptExecutor Js { get; set; }
    }
}