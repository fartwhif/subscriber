using System.Collections.Generic;
using CommandLine;

namespace subscriber
{
    [Verb("backup", HelpText = "Backup your subscriptions.")]
    internal class BackupOptions : ChromeDriverOptions
    {
        [Option('o', "output-file", Required = true, HelpText = "File to save list of subscriptions.")]
        public string OutputFile { get; set; }

        [Option('f', "force-overwrite", Required = false,
            HelpText = "If the output file exists, overwrite it without interactive confirmation.")]
        public bool ForceOverwrite { get; set; }
    }

    [Verb("restore", HelpText = "Restore your subscriptions.")]
    internal class RestoreOptions : ChromeDriverOptions
    {
        [Option('i', "input-file", Required = true,
            HelpText = "Input file containing list of subscriptions to restore.")]
        public string InputFile { get; set; }
    }

    internal class ChromeDriverOptions
    {
        [Option('u', "user-path", Required = true,
            HelpText = @"Chromium user data path. Example: C:\Users\user\AppData\Local\Chromium\User Data")]
        public string UserDataPath { get; set; }
        [Option('d', "driver-path", Required = true,
            HelpText = @"Path to folder containing chromedriver.exe Example: C:\Users\user\Downloads\Win_999972_chrome-win\chrome-win")]
        public string ChromeDriverPath { get; set; }
    }
}