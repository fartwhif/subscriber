﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using CommandLine;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;

namespace subscriber
{
    internal static class Program
    {
        private static bool Completed = false;

        private static void Main(string[] args)
        {
            Parser.Default.ParseArguments<BackupOptions, RestoreOptions>(args)
                .WithParsed<BackupOptions>(_Backup)
                .WithParsed<RestoreOptions>(_Restore)
                .WithNotParsed((arg) => { Completed = true; });
            while (!Completed)
            {
                Thread.Sleep(100);
            }
        }

        private static void _Restore(RestoreOptions opts)
        {
            new Thread(RestoreViaURL).Start(opts);
        }

        private static void _Backup(BackupOptions opts)
        {
            new Thread(Backup).Start(opts);
        }

        private static void Backup(object opts_)
        {
            var opts = (BackupOptions) opts_;
            if (File.Exists(opts.OutputFile))
            {
                if (!opts.ForceOverwrite)
                {
                    PromptYesOrBye($"Output file exists already, overwrite? Y/N");
                }
            }

            var bc = SetupTheBrowser(opts);
            var channels = GetTheChannels(bc);
            File.WriteAllLines(opts.OutputFile, channels);
            bc.Chrome.Quit();
            Completed = true;
        }

        private static void PromptYesOrBye(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                var response = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(response))
                {
                    continue;
                }

                response = response.ToLower().Trim();
                if (response != "y" && response != "n")
                {
                    continue;
                }

                if (response != "y")
                {
                    Environment.Exit(1);
                }

                return;
            }
        }


        private static void RestoreViaURL(object opts_)
        {
            var opts = (RestoreOptions) opts_;
            if (!File.Exists(opts.InputFile))
            {
                Console.WriteLine($"Input file does not exist: {opts.InputFile}");
                Environment.Exit(2);
            }

            var channels = File.ReadAllLines(opts.InputFile).ToList();
            var bc = SetupTheBrowser(opts);
            var chrom = bc.Chrome;
            var js = bc.Js;
            //var existingSubs2 = new List<string>(); //faster testing
            var existingSubs2 = GetTheChannels(bc);
            channels = channels.Except(existingSubs2).ToList();
            foreach (var channel in channels)
            {
                var url = "https://youtube.com" + channel;
                navi(bc, url, false, 1);
                IWebElement subscribeButton = null;
                try
                {
                    subscribeButton = chrom.FindElement(By.XPath(
                        "//*[@id=\"subscribe-button\"]/ytd-subscribe-button-renderer/tp-yt-paper-button"), 3000);
                }
                catch (Exception ex)
                {
                    Debugger.Break();
                }

                if (subscribeButton != null)
                {
                    if (subscribeButton.Text.ToUpper() != "SUBSCRIBE") continue;
                    subscribeButton.Click();
                    Thread.Sleep(300); //give it time to push the update
                }
            }

            bc.Chrome.Quit();
            Completed = true;
        }

        private static void RestoreViaSearch(object opts_)
        {
            var opts = (RestoreOptions) opts_;
            if (!File.Exists(opts.InputFile))
            {
                Console.WriteLine($"Input file does not exist: {opts.InputFile}");
                Environment.Exit(2);
            }

            var channels = File.ReadAllLines(opts.InputFile).ToList();
            var bc = SetupTheBrowser(opts);
            var chrom = bc.Chrome;
            var js = bc.Js;
            //var existingSubs2 = new List<string>(); //faster testing
            var existingSubs2 = GetTheChannels(bc);
            channels = channels.Except(existingSubs2).ToList();
            foreach (var channel in channels)
            {
                Thread.Sleep(1500);
                //var searchInputBox = chrom.FindElement(By.XPath("//input[@id=\"search\"]"));
                var searchInputBox = chrom.FindElement(By.XPath("//input[@id=\"search\"]"), 3000);
                if (searchInputBox == null)
                {
                    throw new Exception("unable to find search input box");
                }

                //chrom.SwitchTo().w Wait.Until(MyConditions.ElementIsVisible(element));
                chrom.ExecuteJavaScript("arguments[0].scrollIntoView(true);", searchInputBox);
                Thread.Sleep(1500);
                searchInputBox.Clear();
                Thread.Sleep(1500);
                searchInputBox.Click();
                Thread.Sleep(1500);
                searchInputBox.SendKeys(channel);
                Thread.Sleep(1500);
                searchInputBox.Submit();
                Thread.Sleep(1500);
                var subscribeButtons = chrom.FindElements(By.XPath(
                    "//*[@id=\"subscribe-button\"]/ytd-subscribe-button-renderer/paper-button/yt-formatted-string"));
                foreach (var subscribeButton in subscribeButtons)
                {
                    if (subscribeButton.Text.ToUpper() != "SUBSCRIBE") continue;
                    var parent = subscribeButton.FindElement(By.XPath("./.."));
                    var effect = parent.GetAttribute("aria-label");
                    var match = Regex.Match(effect, $@"Subscribe\sto\s{Regex.Escape(channel)}\.");
                    if (match.Success)
                    {
                        subscribeButton.Click();
                        Thread.Sleep(1500);
                    }

                    Thread.Sleep(1500);
                }

                Thread.Sleep(1500);
            }

            bc.Chrome.Quit();
            Completed = true;
        }

        private static Browser SetupTheBrowser(ChromeDriverOptions _args)
        {
            var bc = new Browser();
            // start browser, navigate to subscriptions page
            var options = new ChromeOptions();
            // TODO: add option for specifying chrome user data dir
            options.AddArguments(new string[]
            {
                @"--user-data-dir=" + _args.UserDataPath + @"", "--enable-gpu-rasterization",
                "--disable-pings",
                "--no-pings",
                "--flag-switches-begin", "--flag-switches-end"
            });
            bc.Chrome = new ChromeDriver(_args.ChromeDriverPath, options);
            bc.Chrome.SwitchTo();
            Thread.Sleep(1500);
            return navi(bc, "https://youtube.com/feed/subscriptions", true);
        }

        private static Browser navi(Browser bc, string url, bool jQuery, int _wait = 1500)
        {
            bc.Chrome.Navigate().GoToUrl(url);
            Thread.Sleep(_wait);
            bc.Js = (IJavaScriptExecutor) bc.Chrome;
            if (jQuery)
            {
                // inject jQuery
                bc.Js.ExecuteScript(
                    "var headID = document.getElementsByTagName(\"head\")[0];" +
                    "var newScript = document.createElement('script');" +
                    "newScript.type = 'text/javascript';" +
                    "newScript.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js';" +
                    "headID.appendChild(newScript);");
                var wait = new WebDriverWait(bc.Chrome, TimeSpan.FromSeconds(30));
                var until = wait.Until(webDriver =>
                    ((bool) bc.Js.ExecuteScript("return (typeof jQuery != \"undefined\")")));
                bc.Js.ExecuteScript("$.noConflict();");
            }

            return bc;
        }

        private static IEnumerable<string> GetTheChannels(Browser bc)
        {
            var chrom = bc.Chrome;
            var js = bc.Js;
            chrom.FindElement(By.XPath("//*[@id=\"guide-icon\"]")).Click(); // open the left nav menu
            Thread.Sleep(1500);
            js.ExecuteScript(
                $@"var showMoreBtn = jQuery('a[title*=""Show ""]');" +
                "if (showMoreBtn.length > 0) { showMoreBtn.get(0).scrollIntoView(); } ");
            Thread.Sleep(1500);
            js.ExecuteScript(
                $@"var showMoreBtn = jQuery('a[title*=""Show ""]');" +
                "if (showMoreBtn.length > 0) { showMoreBtn[0].click(); } ");
            Thread.Sleep(1400);
            //var existingSubs = chrom.FindElements(By.XPath("//*[@id=\"endpoint\"]/paper-item/yt-formatted-string"));
            var html = js.ExecuteScript("return document.documentElement.outerHTML;");
            //var existingSubs2 = ChannelsExtractFromHtml((string) html).Select(HtmlEntity.DeEntitize);
            js.ExecuteScript("jQuery('app-drawer#guide').removeAttr('opened')"); // close the left nav menu
            var existingSubs2 = ChannelsExtractFromHtml((string) html);
            return existingSubs2;
        }

        private static IEnumerable<string> ChannelsExtractFromHtml(string raw)
        {
            var channels = new List<string>();
            Regex regex = new Regex("tabindex=\"-1\" role=\"tablist\" title=\"([^\"]+)\" href=\"([^\"]+)\">");

            foreach (Match match in regex.Matches(raw))
            {
                var path = match.Groups[2].Value;
                if (path.StartsWith("/c/") || path.StartsWith("/channel/") || path.StartsWith("/user/"))
                {
                    if (path.Contains("/UC4R8DWoMoI7CAwX8_LjQHig") //Live
                        || path.Contains("UCrpQ4p1Ql_hG8rKXIKM1MO") //Fashion & Beauty
                        || path.Contains("UCtFRv9O2AHqOZjjynzrv-xg") //Learning
                        || path.Contains("UCEgdi0XIXXZ-qJOFPf4JSKw") //Sports
                        || path.Contains("UC-9-kyTW8ZkZNDHQJ6FgpwQ") //Music
                        || path.Contains("UClgRkhTL3_hImCAmdLfDE4g") //Movies & Shows
                       )
                    {
                        continue; //fake subs
                    }

                    //whitelist excludes special channel URLs such as /AngryNintendoNerd
                    //what can be done?
                    channels.Add(path);
                }
            }

            return channels;
        }

        private static IEnumerable<string> ChannelsExtractFromHtmlBROKEN(string raw)
        {
            var channels = new List<string>();
            var doc = new HtmlDocument();
            try
            {
                doc.LoadHtml(raw);
                //foreach (HtmlNode playlistCard in doc.DocumentNode.SelectNodes("//*[@id=\"items\"]/ytd-grid-playlist-renderer").DefaultIfEmpty())
                channels.AddRange(from nodes in doc.DocumentNode
                        .SelectNodes(
                            "/html/body/ytd-app/div/app-drawer/div[2]/div/div[2]/div[2]/ytd-guide-renderer/div[1]/ytd-guide-section-renderer[2]")
                        .DefaultIfEmpty()
                    from x in nodes.SelectNodes($@"//*[@id=""endpoint""]/paper-item/yt-formatted-string")
                        .DefaultIfEmpty()
                    select x.InnerText);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return channels;
        }
    }
}